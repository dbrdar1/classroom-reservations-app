const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const db = require('./db.js');

const app = express();

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/slike"));
app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.sendFile(__dirname + "/public/pocetna.html");
});

app.get('/zauzeca', function(req, res) {
    let objekatZaSlanje = {
        periodicna: [],
        vanredna: []
    };
    db.rezervacija.findAll().then(function(vraceneSveRezervacije) {
        let brojacObradjenih = 0;
        for (let i = 0; i < vraceneSveRezervacije.length; i++) {
            let nadjenaRezervacija = vraceneSveRezervacije[i];
            db.sala.findOne({where: {id: nadjenaRezervacija.sala}}).then(function(nadjenaSala) {
                db.termin.findOne({where: {id: nadjenaRezervacija.termin}}).then(function(nadjeniTermin) {
                    db.osoblje.findOne({where: {id: nadjenaRezervacija.osoba}}).then(function(nadjenaOsoba) {
                        let noviObjekatZauzeca = {};
                        let vracenoVrijeme = nadjeniTermin.pocetak;
                        let nizZaUredjivanje = vracenoVrijeme.split(":");
                        let uredjenoVrijemePocetka = nizZaUredjivanje[0] + ":" + nizZaUredjivanje[1];
                        vracenoVrijeme = nadjeniTermin.kraj;
                        nizZaUredjivanje = vracenoVrijeme.split(":");
                        let uredjenoVrijemeKraja = nizZaUredjivanje[0] + ":" + nizZaUredjivanje[1];
                        if (nadjeniTermin.redovni == true) {
                            noviObjekatZauzeca.dan = nadjeniTermin.dan;
                            noviObjekatZauzeca.semestar = nadjeniTermin.semestar;
                            noviObjekatZauzeca.pocetak = uredjenoVrijemePocetka;
                            noviObjekatZauzeca.kraj = uredjenoVrijemeKraja;
                            noviObjekatZauzeca.naziv = nadjenaSala.naziv;
                            noviObjekatZauzeca.predavac = nadjenaOsoba.ime + " " + nadjenaOsoba.prezime;
                            objekatZaSlanje.periodicna.push(noviObjekatZauzeca);
                        }
                        else {
                            noviObjekatZauzeca.datum = nadjeniTermin.datum;
                            noviObjekatZauzeca.pocetak = uredjenoVrijemePocetka;
                            noviObjekatZauzeca.kraj = uredjenoVrijemeKraja;
                            noviObjekatZauzeca.naziv = nadjenaSala.naziv;
                            noviObjekatZauzeca.predavac = nadjenaOsoba.ime + " " + nadjenaOsoba.prezime;
                            objekatZaSlanje.vanredna.push(noviObjekatZauzeca);
                        }
                        brojacObradjenih++;
                        if (brojacObradjenih == vraceneSveRezervacije.length) {
                            res.json(objekatZaSlanje);
                            return;
                        }
                    }).catch(function(err) {
                        console.log("Greska " + err);
                    });
                }).catch(function(err) {
                    console.log("Greska " + err);
                });
            }).catch(function(err) {
                console.log("Greska " + err);
            });
        }
    }).catch(function(err) {
        console.log("Greska " + err);
    });
});

function uporediVremena(vrijemePrvo, vrijemeDrugo) {

    if (vrijemePrvo === vrijemeDrugo) return 0;
    let nizPrvi = vrijemePrvo.split(":");
    let nizDrugi = vrijemeDrugo.split(":");
    if (eval(nizPrvi[0]) > eval(nizDrugi[0])) {
        return 1;
    }
    else if (eval(nizPrvi[0]) === eval(nizDrugi[0]) && eval(nizPrvi[1]) > eval(nizDrugi[1])) return 1;
    else return -1;

}

function jeLiVrijemeIzmedjuDrugaDva(prvoVrijeme, drugoVrijeme, treceVrijeme) {

    return uporediVremena(prvoVrijeme, drugoVrijeme) >= 0 && uporediVremena(prvoVrijeme, treceVrijeme) <= 0;

}

app.post('/zauzeca', function(req, res) {
    let tijelo = req.body;
    let danUMjesecu = tijelo['danUMjesecu'];
    let danUSedmici = tijelo['danUSedmici'];
    let mjesec = tijelo['mjesec'];
    let sala = tijelo['sala'];
    let pocetak = tijelo['pocetak'];
    let kraj = tijelo['kraj'];
    let periodicnaRezervacija = tijelo['periodicnaRezervacija'];
    let imeIPrezimeOsobe = tijelo['imeIPrezimeOsobe'];
    let ulogaOsobe = tijelo['ulogaOsobe'];

    let objekatZaSlanje = {
        periodicna: [],
        vanredna: []
    };

    db.rezervacija.findAll().then(function(vraceneSveRezervacije) {
        let brojacObradjenihRezervacijaPrvo = 0;
        for (let i = 0; i < vraceneSveRezervacije.length; i++) {
            let nadjenaRezervacija = vraceneSveRezervacije[i];
            db.osoblje.findOne({where: {id: nadjenaRezervacija.osoba}}).then(function(nadjenaOsoba) {
                let idNadjeneOsobe = nadjenaOsoba.id;
                db.sala.findOne({where: {id: nadjenaRezervacija.sala}}).then(function(nadjenaSala) {
                    let idNadjeneSale = nadjenaSala.id;
                    db.termin.findOne({where: {id: nadjenaRezervacija.termin}}).then(function(nadjeniTermin) {
                        let noviObjekatZauzeca = {};
                        let vracenoVrijeme = nadjeniTermin.pocetak;
                        let nizZaUredjivanje = vracenoVrijeme.split(":");
                        let uredjenoVrijemePocetka = nizZaUredjivanje[0] + ":" + nizZaUredjivanje[1];
                        vracenoVrijeme = nadjeniTermin.kraj;
                        nizZaUredjivanje = vracenoVrijeme.split(":");
                        let uredjenoVrijemeKraja = nizZaUredjivanje[0] + ":" + nizZaUredjivanje[1];
                        if (nadjeniTermin.redovni == true) {
                            noviObjekatZauzeca.dan = nadjeniTermin.dan;
                            noviObjekatZauzeca.semestar = nadjeniTermin.semestar;
                            noviObjekatZauzeca.pocetak = uredjenoVrijemePocetka;
                            noviObjekatZauzeca.kraj = uredjenoVrijemeKraja;
                            noviObjekatZauzeca.naziv = nadjenaSala.naziv;
                            noviObjekatZauzeca.predavac = nadjenaOsoba.ime + " " + nadjenaOsoba.prezime;
                            noviObjekatZauzeca.ulogaPredavaca = nadjenaOsoba.uloga;
                            noviObjekatZauzeca.idPredavaca = idNadjeneOsobe;
                            noviObjekatZauzeca.idSale = idNadjeneSale;
                            objekatZaSlanje.periodicna.push(noviObjekatZauzeca);
                        }
                        else {
                            noviObjekatZauzeca.datum = nadjeniTermin.datum;
                            noviObjekatZauzeca.pocetak = uredjenoVrijemePocetka;
                            noviObjekatZauzeca.kraj = uredjenoVrijemeKraja;
                            noviObjekatZauzeca.naziv = nadjenaSala.naziv;
                            noviObjekatZauzeca.predavac = nadjenaOsoba.ime + " " + nadjenaOsoba.prezime;
                            noviObjekatZauzeca.ulogaPredavaca = nadjenaOsoba.uloga;
                            noviObjekatZauzeca.idPredavaca = idNadjeneOsobe;
                            noviObjekatZauzeca.idSale = idNadjeneSale;
                            objekatZaSlanje.vanredna.push(noviObjekatZauzeca);
                        }
                        brojacObradjenihRezervacijaPrvo++;
                        if (brojacObradjenihRezervacijaPrvo == vraceneSveRezervacije.length) {
                            let vraceniObjekatZauzeca = objekatZaSlanje;

                            let periodicnaZauzeca = vraceniObjekatZauzeca.periodicna;
                            let vanrednaZauzeca = vraceniObjekatZauzeca.vanredna;
                            
                            let trenutnaGodina = new Date().getFullYear();
                            let brojDanaMjeseca = new Date(trenutnaGodina, mjesec + 1, 0).getDate();
                            let i;
                            // Provjera periodicnih zauzeca
                            let filtriraniNizPeriodicnihZauzeca = periodicnaZauzeca.filter(function(zauzece) {
                                let danZauzeca = zauzece.dan;
                                let danSedmice = danUSedmici;
                                if (danSedmice === 0) danSedmice = 7;
                                return zauzece.naziv === sala && danSedmice === danZauzeca + 1
                                && (jeLiVrijemeIzmedjuDrugaDva(zauzece.pocetak, pocetak, kraj) || jeLiVrijemeIzmedjuDrugaDva(zauzece.kraj, pocetak, kraj)
                                || jeLiVrijemeIzmedjuDrugaDva(pocetak, zauzece.pocetak, zauzece.kraj) || jeLiVrijemeIzmedjuDrugaDva(kraj, zauzece.pocetak, zauzece.kraj))
                                && ((zauzece.semestar === "zimski" && (mjesec === 0 || mjesec >= 9 && mjesec <= 11)) 
                                || (zauzece.semestar === "ljetni" && (mjesec >= 1 && mjesec <= 5))) 
                                && (!((uporediVremena(zauzece.kraj, pocetak) == 0 && uporediVremena(zauzece.pocetak, zauzece.kraj) <= 0) 
                                || (uporediVremena(kraj, zauzece.pocetak) == 0 && uporediVremena(pocetak, kraj) <= 0)));
                            });
                            if (filtriraniNizPeriodicnihZauzeca.length != 0) {
                                let kriticnaOsoba = {};
                                kriticnaOsoba.imeIPrezime = filtriraniNizPeriodicnihZauzeca[0].predavac;
                                kriticnaOsoba.uloga = filtriraniNizPeriodicnihZauzeca[0].ulogaPredavaca;
                                res.set('Content-Type', 'text/plain; charset=utf-8');
                                let praviMjesec = mjesec + 1;
                                let porukaGreske = "Nije moguće rezervisati salu " + sala + " za navedeni datum " + 
                                ((danUMjesecu < 10) ? ("0" + danUMjesecu) : danUMjesecu) + "/" + ((praviMjesec < 10) ? ("0" + praviMjesec) : praviMjesec) 
                                + "/" + trenutnaGodina + " i termin od " + pocetak + " do " + kraj + "! Salu je već rezervisala osoba (" + kriticnaOsoba.uloga 
                                + ") " + kriticnaOsoba.imeIPrezime + "!";
                                res.status(200).send(porukaGreske);
                                return;
                            }
                            // Provjera vanrednih zauzeca
                            if (!periodicnaRezervacija) {
                                let filtriraniNizVanrednihZauzeca = vanrednaZauzeca.filter(function(zauzece) {
                                    let nizRazdvojen = zauzece.datum.split(".");
                                    nizRazdvojen.reverse();
                                    let noviDatumFormat = nizRazdvojen.join("-");
                                    let datumZauzeca = new Date(noviDatumFormat);
                                    return datumZauzeca.getFullYear() == trenutnaGodina 
                                    && datumZauzeca.getMonth() == mjesec
                                    && datumZauzeca.getDate() == danUMjesecu
                                    && zauzece.naziv === sala 
                                    && (jeLiVrijemeIzmedjuDrugaDva(zauzece.pocetak, pocetak, kraj) || jeLiVrijemeIzmedjuDrugaDva(zauzece.kraj, pocetak, kraj)
                                    || jeLiVrijemeIzmedjuDrugaDva(pocetak, zauzece.pocetak, zauzece.kraj) || jeLiVrijemeIzmedjuDrugaDva(kraj, zauzece.pocetak, zauzece.kraj))
                                    && (!((uporediVremena(zauzece.kraj, pocetak) == 0 && uporediVremena(zauzece.pocetak, zauzece.kraj) <= 0) 
                                    || (uporediVremena(kraj, zauzece.pocetak) == 0 && uporediVremena(pocetak, kraj) <= 0)));
                                });
                                if (filtriraniNizVanrednihZauzeca.length != 0) {
                                    let kriticnaOsoba = {};
                                    kriticnaOsoba.imeIPrezime = filtriraniNizVanrednihZauzeca[0].predavac;
                                    kriticnaOsoba.uloga = filtriraniNizVanrednihZauzeca[0].ulogaPredavaca;
                                    res.set('Content-Type', 'text/plain; charset=utf-8');
                                    let praviMjesec = mjesec + 1;
                                    let porukaGreske = "Nije moguće rezervisati salu " + sala + " za navedeni datum " + 
                                    ((danUMjesecu < 10) ? ("0" + danUMjesecu) : danUMjesecu) + "/" + ((praviMjesec < 10) ? ("0" + praviMjesec) : praviMjesec) + "/" + trenutnaGodina + 
                                    " i termin od " + pocetak + " do " + kraj + "! Salu je već rezervisala osoba (" + kriticnaOsoba.uloga 
                                    + ") " + kriticnaOsoba.imeIPrezime + "!";
                                    res.status(200).send(porukaGreske);
                                    return;
                                }
                            }
                            else {
                                let filtriraniNizVanrednihZauzeca = vanrednaZauzeca.filter(function(zauzece) {
                                    let nizRazdvojen = zauzece.datum.split(".");
                                    nizRazdvojen.reverse();
                                    let noviDatumFormat = nizRazdvojen.join("-");
                                    let datumZauzeca = new Date(noviDatumFormat);
                                    let danSedmice = danUSedmici;
                                    if (danSedmice === 0) danSedmice = 7;
                                    let danZauzeca = datumZauzeca.getDay();
                                    if (danZauzeca == 0) danZauzeca = 7;
                                    let mjesecZauzeca = datumZauzeca.getMonth();
                                    return danZauzeca == danSedmice && zauzece.naziv === sala 
                                    && (((mjesecZauzeca === 0 || mjesecZauzeca >= 9 && mjesecZauzeca <= 11) && (mjesec === 0 || mjesec >= 9 && mjesec <= 11)) 
                                    || ((mjesecZauzeca >= 1 && mjesecZauzeca <= 5) && (mjesec >= 1 && mjesec <= 5))) 
                                    && (jeLiVrijemeIzmedjuDrugaDva(zauzece.pocetak, pocetak, kraj) || jeLiVrijemeIzmedjuDrugaDva(zauzece.kraj, pocetak, kraj)
                                    || jeLiVrijemeIzmedjuDrugaDva(pocetak, zauzece.pocetak, zauzece.kraj) || jeLiVrijemeIzmedjuDrugaDva(kraj, zauzece.pocetak, zauzece.kraj))
                                    && (!((uporediVremena(zauzece.kraj, pocetak) == 0 && uporediVremena(zauzece.pocetak, zauzece.kraj) <= 0) 
                                    || (uporediVremena(kraj, zauzece.pocetak) == 0 && uporediVremena(pocetak, kraj) <= 0)));
                                });
                                if (filtriraniNizVanrednihZauzeca.length != 0) {
                                    let kriticnaOsoba = {};
                                    kriticnaOsoba.imeIPrezime = filtriraniNizVanrednihZauzeca[0].predavac;
                                    kriticnaOsoba.uloga = filtriraniNizVanrednihZauzeca[0].ulogaPredavaca;
                                    res.set('Content-Type', 'text/plain; charset=utf-8');
                                    let praviMjesec = mjesec + 1;
                                    let porukaGreske = "Nije moguće rezervisati salu " + sala + " za navedeni datum " + 
                                    ((danUMjesecu < 10) ? ("0" + danUMjesecu) : danUMjesecu) + "/" + ((praviMjesec < 10) ? ("0" + praviMjesec) : praviMjesec) + "/" + trenutnaGodina + 
                                    " i termin od " + pocetak + " do " + kraj + "! Salu je već rezervisala osoba (" + kriticnaOsoba.uloga 
                                    + ") " + kriticnaOsoba.imeIPrezime + "!";
                                    res.status(200).send(porukaGreske);
                                    return;
                                }
                            }
                            // Sve uredu, dodajemo novo zauzece
                            if (periodicnaRezervacija) {

                                if (danUSedmici === 0) danUSedmici = 7;
                                danUSedmici--;

                                let semestarZaDodati = "zimski";
                                if (mjesec >= 1 && mjesec <= 5) semestarZaDodati = "ljetni";

                                let objekatNovogPeriodicnogZauzeca = {
                                    dan: danUSedmici,
                                    semestar: semestarZaDodati,
                                    pocetak: pocetak,
                                    kraj: kraj,
                                    naziv: sala,
                                    predavac: imeIPrezimeOsobe
                                };

                                vraceniObjekatZauzeca.periodicna.push(objekatNovogPeriodicnogZauzeca);

                                let dodavanjeTerminaNizPromisea = [];
                                let dodavanjeRezervacijeNizPromisea = [];
                                db.sala.findOne({where: {naziv: sala}}).then(function(nadjenaSalaZaDodavanje) {
                                    let idOdabraneSale = nadjenaSalaZaDodavanje.id;
                                    let nizImenaPrezimena = imeIPrezimeOsobe.split(" ");
                                    let imeOdvojeno = nizImenaPrezimena[0];
                                    let prezimeOdvojeno = nizImenaPrezimena[1];
                                    db.osoblje.findOne({where: {ime:imeOdvojeno, prezime: prezimeOdvojeno}}).then(function(nadjenaOsobaZaDodavanje) {
                                        let idOdabraneOsobe = nadjenaOsobaZaDodavanje.id;
                                        dodavanjeTerminaNizPromisea.push(db.termin.create({redovni: true, dan: danUSedmici, datum: null, semestar: semestarZaDodati, pocetak: pocetak, kraj: kraj}));
                                        Promise.all(dodavanjeTerminaNizPromisea).then(function(vraceniTermini) {
                                            dodavanjeRezervacijeNizPromisea.push(db.rezervacija.create({termin: vraceniTermini[0].id, sala: idOdabraneSale, osoba: idOdabraneOsobe}));
                                            Promise.all(dodavanjeRezervacijeNizPromisea).then(function(vraceneRezervacije) {
                                                res.json(vraceniObjekatZauzeca);
                                            }).catch(function(err) {
                                                console.log("Greska " + err);
                                            });
                                        }).catch(function(err) {
                                            console.log("Greska " + err);
                                        });
                                    }).catch(function(err) {
                                        console.log("Greska " + err);
                                    });
                                }).catch(function(err) {
                                    console.log("Greska " + err);
                                });
                            }
                            else {
                                let praviMjesec = mjesec + 1;
                                let stringDatumaZaDodati = ((danUMjesecu < 10) ? ("0" + danUMjesecu) : danUMjesecu) +
                                "." + ((praviMjesec < 10) ? ("0" + praviMjesec) : praviMjesec) + "." + new Date().getFullYear();
                                
                                let objekatNovogVanrednogZauzeca = {
                                    datum: stringDatumaZaDodati,
                                    pocetak: pocetak,
                                    kraj: kraj,
                                    naziv: sala,
                                    predavac: imeIPrezimeOsobe
                                };

                                vraceniObjekatZauzeca.vanredna.push(objekatNovogVanrednogZauzeca);

                                let dodavanjeTerminaNizPromisea = [];
                                let dodavanjeRezervacijeNizPromisea = [];
                                db.sala.findOne({where: {naziv: sala}}).then(function(nadjenaSalaZaDodavanje) {
                                    let idOdabraneSale = nadjenaSalaZaDodavanje.id;
                                    let nizImenaPrezimena = imeIPrezimeOsobe.split(" ");
                                    let imeOdvojeno = nizImenaPrezimena[0];
                                    let prezimeOdvojeno = nizImenaPrezimena[1];
                                    db.osoblje.findOne({where: {ime:imeOdvojeno, prezime: prezimeOdvojeno}}).then(function(nadjenaOsobaZaDodavanje) {
                                        let idOdabraneOsobe = nadjenaOsobaZaDodavanje.id;
                                        dodavanjeTerminaNizPromisea.push(db.termin.create({redovni: false, dan: null, datum: stringDatumaZaDodati, semestar: null, pocetak: pocetak, kraj: kraj}));
                                        Promise.all(dodavanjeTerminaNizPromisea).then(function(vraceniTermini) {
                                            dodavanjeRezervacijeNizPromisea.push(db.rezervacija.create({termin: vraceniTermini[0].id, sala: idOdabraneSale, osoba: idOdabraneOsobe}));
                                            Promise.all(dodavanjeRezervacijeNizPromisea).then(function(vraceneRezervacije) {
                                                res.json(vraceniObjekatZauzeca);
                                            }).catch(function(err) {
                                                console.log("Greska " + err);
                                            });
                                        }).catch(function(err) {
                                            console.log("Greska " + err);
                                        });
                                    }).catch(function(err) {
                                        console.log("Greska " + err);
                                    });
                                }).catch(function(err) {
                                    console.log("Greska " + err);
                                });
                            }
                        }
                    }).catch(function(err) {
                        console.log("Greska " + err);
                    });
                }).catch(function(err) {
                    console.log("Greska " + err);
                });
            }).catch(function(err) {
                console.log("Greska " + err);
            });
        }
    }).catch(function(err) {
        console.log("Greska " + err);
    });
});


app.post('/slike', function(req, res) {

    let tijelo = req.body;
    let nizVecPostojecihSlika = tijelo['nizVecPostojecihSlika'];

    let objekatZaSlanje = {
        nizSlika: [],
        finalneSlikePoslane: false
    };

    let putanja = __dirname + "/slike";

    fs.readdir(putanja, function(greska, slike) {

        if (greska) return console.error(greska);

        let brojacSlika = 0;
        let nadjenaJosJednaSlika = false;
        for (let i = 0; i < slike.length; i++) {
            let trenutnaSlika = slike[i];
            if (!nizVecPostojecihSlika.includes(trenutnaSlika)) {
                brojacSlika++;
                if (brojacSlika <= 3) objekatZaSlanje.nizSlika.push(trenutnaSlika);
                else if (brojacSlika == 4) {
                    nadjenaJosJednaSlika = true;
                    break;
                }
            }
        }

        if (!nadjenaJosJednaSlika) objekatZaSlanje.finalneSlikePoslane = true;

        res.json(objekatZaSlanje);
    });
});

app.get('/sale', function(req, res) {
    db.sala.findAll().then(function(vracenoSve) {
        let objekatZaSlanje = {
            nizSala: []
        };
        let nizTemp = [];
        for (let i = 0; i < vracenoSve.length; i++) {
            let vraceniNaziv = vracenoSve[i].naziv;
            nizTemp.push({naziv: vraceniNaziv});
        }
        objekatZaSlanje.nizSala = nizTemp;
        res.json(objekatZaSlanje);
    }).catch(function(err) {
        console.log("Greska " + err);
    });
});

app.get('/osoblje', function(req, res) {
    db.osoblje.findAll().then(function(vracenoSve) {
        let objekatZaSlanje = {
            nizOsoblja: []
        };
        let nizTemp = [];
        for (let i = 0; i < vracenoSve.length; i++) {
            let vracenoIme = vracenoSve[i].ime;
            let vracenoPrezime = vracenoSve[i].prezime;
            let vracenaUloga = vracenoSve[i].uloga;
            nizTemp.push({ime: vracenoIme, prezime: vracenoPrezime, uloga: vracenaUloga});
        }
        objekatZaSlanje.nizOsoblja = nizTemp;
        res.json(objekatZaSlanje);
    }).catch(function(err) {
        console.log("Greska " + err);
    });
});



function dajTrenutnoVrijeme() {
    let date = new Date();
    let sati = date.getHours();
    let minute = date.getMinutes();

    let stvarnoHH = (sati < 10) ? ('0' + sati) : sati; 
    let stvarnoMM = (minute < 10) ? ('0' + minute) : minute;

    let stvarnoVrijeme = stvarnoHH + ":" + stvarnoMM;
    return stvarnoVrijeme;
}

function provjeriJeLiOsobaTrenutnoUSali(vraceniTermin) {
    if (vraceniTermin.redovni == true) {
        let danasnjiDatum = new Date();
        let danasnjiDanSedmice = danasnjiDatum.getDay();
        if (danasnjiDanSedmice == 0) danasnjiDanSedmice = 7;
        let danSedmiceVracenogTermina = vraceniTermin.dan + 1;
        if (danasnjiDanSedmice == danSedmiceVracenogTermina) {
            let danasnjiSemestar = "zimski";
            let danasnjiMjesec = danasnjiDatum.getMonth() + 1;
            if (danasnjiMjesec >= 7 && danasnjiMjesec <= 9) return false;
            if (danasnjiMjesec >= 2 && danasnjiMjesec <= 6) danasnjiSemestar = "ljetni";
            let semestarVracenogTermina = vraceniTermin.semestar;
            if (danasnjiSemestar == semestarVracenogTermina) {
                let vracenoVrijeme = vraceniTermin.pocetak;
                let nizZaUredjivanje = vracenoVrijeme.split(":");
                let uredjenoVrijemePocetka = nizZaUredjivanje[0] + ":" + nizZaUredjivanje[1];
                vracenoVrijeme = vraceniTermin.kraj;
                nizZaUredjivanje = vracenoVrijeme.split(":");
                let uredjenoVrijemeKraja = nizZaUredjivanje[0] + ":" + nizZaUredjivanje[1];
                return jeLiVrijemeIzmedjuDrugaDva(dajTrenutnoVrijeme(), uredjenoVrijemePocetka, uredjenoVrijemeKraja);
            }
            else return false;
        }
        else return false;
    }
    else {
        let datumVracenogTermina = vraceniTermin.datum;
        let nizVrijednostiVracenogTermina = datumVracenogTermina.split(".");
        let danMjesecaVracenogTermina = parseInt(nizVrijednostiVracenogTermina[0]);
        let mjesecVracenogTermina = parseInt(nizVrijednostiVracenogTermina[1]);
        let godinaVracenogTermina = parseInt(nizVrijednostiVracenogTermina[2]);
        let danasnjiDatum = new Date();
        let danasnjiDanMjeseca = danasnjiDatum.getDate();
        let danasnjiMjesec = danasnjiDatum.getMonth() + 1;
        let danasnjaGodina = danasnjiDatum.getFullYear();
        if (danasnjiDanMjeseca == danMjesecaVracenogTermina && danasnjiMjesec == mjesecVracenogTermina && danasnjaGodina == godinaVracenogTermina) {
            let vracenoVrijeme = vraceniTermin.pocetak;
            let nizZaUredjivanje = vracenoVrijeme.split(":");
            let uredjenoVrijemePocetka = nizZaUredjivanje[0] + ":" + nizZaUredjivanje[1];
            vracenoVrijeme = vraceniTermin.kraj;
            nizZaUredjivanje = vracenoVrijeme.split(":");
            let uredjenoVrijemeKraja = nizZaUredjivanje[0] + ":" + nizZaUredjivanje[1];
            return jeLiVrijemeIzmedjuDrugaDva(dajTrenutnoVrijeme(), uredjenoVrijemePocetka, uredjenoVrijemeKraja);
        }
        else {
            return false;
        }
    }
}

app.get('/pozicijeosoba', function(req, res) {
    let objekatZaSlanje = {
        nizTrojki: []
    };
    db.osoblje.findAll().then(function(vracenoSvoOsoblje) {
        let vecVraceno = false;
        let brojacObradjenogOsoblja = 0;
        for (let i = 0; i < vracenoSvoOsoblje.length; i++) {
            let trenutnaOsoba = vracenoSvoOsoblje[i];
            let trojkaZaTrenutnuOsobu = {};
            trojkaZaTrenutnuOsobu.imeIPrezime = trenutnaOsoba.ime + " " + trenutnaOsoba.prezime;
            trojkaZaTrenutnuOsobu.uloga = trenutnaOsoba.uloga;
            trenutnaOsoba.getRezervacijeOsoblja().then(function(vraceneRezervacijeOsobe) {
                if (vraceneRezervacijeOsobe.length == 0) {
                    trojkaZaTrenutnuOsobu.gdjeJeTrenutno = "u kancelariji";
                    objekatZaSlanje.nizTrojki.push(trojkaZaTrenutnuOsobu);
                    brojacObradjenogOsoblja++;
                    if (brojacObradjenogOsoblja == vracenoSvoOsoblje.length) {
                        if (!vecVraceno) {
                            res.json(objekatZaSlanje);
                            vecVraceno = true;
                        }
                        return;
                    }
                }
                let brojacObradjenihRezervacija = 0;
                for (let j = 0; j < vraceneRezervacijeOsobe.length; j++) {
                    let trenutnaRezervacijaOsobe = vraceneRezervacijeOsobe[j];
                    db.sala.findOne({where: {id: trenutnaRezervacijaOsobe.sala}}).then(function(vracenaSala) {
                        db.termin.findOne({where: {id: trenutnaRezervacijaOsobe.termin}}).then(function(vraceniTermin) {
                            let osobaJeTrenutnoUSali = provjeriJeLiOsobaTrenutnoUSali(vraceniTermin);
                            brojacObradjenihRezervacija++;
                            if (brojacObradjenihRezervacija == vraceneRezervacijeOsobe.length) brojacObradjenogOsoblja++;
                            if (osobaJeTrenutnoUSali) {
                                trojkaZaTrenutnuOsobu.gdjeJeTrenutno = "u sali " + vracenaSala.naziv;
                                objekatZaSlanje.nizTrojki.push(trojkaZaTrenutnuOsobu);
                                if (brojacObradjenogOsoblja == vracenoSvoOsoblje.length) {
                                    if (!vecVraceno) {
                                        res.json(objekatZaSlanje);
                                        vecVraceno = true;
                                    }
                                }
                                return;
                            }
                            if (brojacObradjenihRezervacija == vraceneRezervacijeOsobe.length) {
                                trojkaZaTrenutnuOsobu.gdjeJeTrenutno = "u kancelariji";
                                objekatZaSlanje.nizTrojki.push(trojkaZaTrenutnuOsobu);
                            }
                            if (brojacObradjenogOsoblja == vracenoSvoOsoblje.length && brojacObradjenihRezervacija == vraceneRezervacijeOsobe.length) {
                                if (!vecVraceno) {
                                    res.json(objekatZaSlanje);
                                    vecVraceno = true;
                                }
                                return;
                            }
                        }).catch(function(err) {
                            console.log("Greska termin " + err);
                        });
                    }).catch(function(err) {
                        console.log("Greska sala " + err);
                    });
                }
            }).catch(function(err) {
                console.log("Greska rezerv " + err);
            });
        }
    }).catch(function(err) {
        console.log("Greska osoba " + err);
    });
});


function ubaciPocetnePodatkeUBazu() {
    let osobljeListaPromisea = [];
    let salaListaPromisea = [];
    let terminListaPromisea = [];
    let rezervacijaListaPromisea = [];
    return new Promise(function(resolve, reject) {
        osobljeListaPromisea.push(db.osoblje.create({id: 1, ime:'Neko', prezime: 'Nekić', uloga: 'profesor'}));
        osobljeListaPromisea.push(db.osoblje.create({id: 2, ime:'Drugi', prezime: 'Neko', uloga: 'asistent'}));
        osobljeListaPromisea.push(db.osoblje.create({id: 3, ime:'Test', prezime: 'Test', uloga: 'asistent'}));
        Promise.all(osobljeListaPromisea).then(function(vracenoOsoblje) {
            salaListaPromisea.push(db.sala.create({id: 1, naziv: '1-11', zaduzenaOsoba: 1}));
            salaListaPromisea.push(db.sala.create({id: 2, naziv: '1-15', zaduzenaOsoba: 2}));
            Promise.all(salaListaPromisea).then(function(vraceneSale) {
                terminListaPromisea.push(db.termin.create({id: 1, redovni: false, dan: null, datum: '01.01.2020', semestar: null, pocetak: '12:00', kraj: '13:00'}));
                terminListaPromisea.push(db.termin.create({id: 2, redovni: true, dan: 0, datum: null, semestar: 'zimski', pocetak: '13:00', kraj: '14:00'}));
                Promise.all(terminListaPromisea).then(function(vraceniTermini) {
                    rezervacijaListaPromisea.push(db.rezervacija.create({id: 1, termin: 1, sala: 1, osoba: 1}));
                    rezervacijaListaPromisea.push(db.rezervacija.create({id: 2, termin: 2, sala: 1, osoba: 3}));
                    Promise.all(rezervacijaListaPromisea).then(function(vraceneRezervacije) {
                        resolve(vraceneRezervacije);
                    }).catch(function(err) {
                        console.log("Greska rezervacije " + err);
                    });
                }).catch(function(err) {
                    console.log("Greska termini " + err);
                });
            }).catch(function(err) {
                console.log("Greska sale " + err);
            });
        }).catch(function(err) {
            console.log("Greska osoblje " + err);
        });
    });
}

db.sequelize.sync({force: true}).then(function() {
    ubaciPocetnePodatkeUBazu().then(function() {
        console.log("Uspjesno ubacivanje pocetnih podataka!");
        app.listen(8080);
        app.emit("sad mogu testovi");
    }).catch(function(err) {
        console.log("Greska baze " + err);
    });
});

module.exports = app;