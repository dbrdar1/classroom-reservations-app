const index = require("../index.js");
const chai = require("chai");
const chaiHttp = require("chai-http");

const {expect} = chai;
chai.use(chaiHttp);

before(function(done) {
    index.on('sad mogu testovi', function() {
        done();
    })
});

describe("Test 1", () => {
    it ("Treba dohvatiti ispravno imena, prezimena i uloge svih osoba iz baze", function(done) {
        chai
            .request(index)
            .get("/osoblje")
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('nizOsoblja').with.lengthOf(3);
                let nizVracenihImenaIPrezimena = [];
                res.body.nizOsoblja.forEach(pojedinacnaOsoba => {
                    nizVracenihImenaIPrezimena.push(pojedinacnaOsoba.ime + " " + pojedinacnaOsoba.prezime + ", " + pojedinacnaOsoba.uloga);
                });
                expect(nizVracenihImenaIPrezimena).to.include("Neko Nekić, profesor");
                expect(nizVracenihImenaIPrezimena).to.include("Drugi Neko, asistent");
                expect(nizVracenihImenaIPrezimena).to.include("Test Test, asistent");
                done();
            });
    });
});

describe("Test 2", () => {
    it ("Treba dohvatiti ispravno nazive svih sala", function(done) {
        chai
            .request(index)
            .get("/sale")
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('nizSala').with.lengthOf(2);
                let nizVracenihNazivaSala = [];
                res.body.nizSala.forEach(pojedinacnaSala => {
                    nizVracenihNazivaSala.push(pojedinacnaSala.naziv);
                });
                expect(nizVracenihNazivaSala).to.include("1-11");
                expect(nizVracenihNazivaSala).to.include("1-15");
                done();
            });
    });
});

describe("Test 3", () => {
    it ("Treba ispravno dohvatiti sva zauzeca", function(done) {
        chai
            .request(index)
            .get("/zauzeca")
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('periodicna').with.lengthOf(1);
                expect(res.body).to.have.property('vanredna').with.lengthOf(1);
                let dohvacenoRedovnoZauzece = res.body.periodicna[0]; 
                expect(dohvacenoRedovnoZauzece.dan).to.equal(0);
                expect(dohvacenoRedovnoZauzece.semestar).to.equal("zimski");
                expect(dohvacenoRedovnoZauzece.pocetak).to.equal("13:00");
                expect(dohvacenoRedovnoZauzece.kraj).to.equal("14:00");
                expect(dohvacenoRedovnoZauzece.naziv).to.equal("1-11");
                let dohvacenoVanrednoZauzece = res.body.vanredna[0];
                expect(dohvacenoVanrednoZauzece.datum).to.equal("01.01.2020");
                expect(dohvacenoVanrednoZauzece.pocetak).to.equal("12:00");
                expect(dohvacenoVanrednoZauzece.kraj).to.equal("13:00");
                expect(dohvacenoVanrednoZauzece.naziv).to.equal("1-11");
                done();
            });
    });
});

describe("Test 4", () => {
    it ("Treba ispravno azurirati sva zauzeca nakon dodavanja nove vanredne rezervacije", function(done) {
        let objekatZaPostRequest = {
            danUMjesecu: 9,
            danUSedmici: 4,
            mjesec: 0,
            sala: "1-15",
            pocetak: "13:30",
            kraj: "15:00",
            periodicnaRezervacija: false,
            imeIPrezimeOsobe: "Drugi Neko",
            ulogaOsobe: "asistent"
        };
        chai
            .request(index)
            .post("/zauzeca")
            .send(objekatZaPostRequest)
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('periodicna').with.lengthOf(1);
                expect(res.body).to.have.property('vanredna').with.lengthOf(2);
                let dohvacenoRedovnoZauzece = res.body.periodicna[0];
                expect(dohvacenoRedovnoZauzece.dan).to.equal(0);
                expect(dohvacenoRedovnoZauzece.semestar).to.equal("zimski");
                expect(dohvacenoRedovnoZauzece.pocetak).to.equal("13:00");
                expect(dohvacenoRedovnoZauzece.kraj).to.equal("14:00");
                expect(dohvacenoRedovnoZauzece.naziv).to.equal("1-11");
                let nizDohvacenihVanrednihZauzeca = res.body.vanredna;
                let nizDohvacenihDatuma = [];
                let nizDohvacenihPocetaka = [];
                let nizDohvacenihKrajeva = [];
                let nizDohvacenihNaziva = [];
                nizDohvacenihVanrednihZauzeca.forEach(pojedinacnoZauzece => {
                    nizDohvacenihDatuma.push(pojedinacnoZauzece.datum);
                    nizDohvacenihPocetaka.push(pojedinacnoZauzece.pocetak);
                    nizDohvacenihKrajeva.push(pojedinacnoZauzece.kraj);
                    nizDohvacenihNaziva.push(pojedinacnoZauzece.naziv);
                });
                expect(nizDohvacenihDatuma).to.include("01.01.2020");
                expect(nizDohvacenihDatuma).to.include("09.01.2020");
                expect(nizDohvacenihPocetaka).to.include("12:00");
                expect(nizDohvacenihPocetaka).to.include("13:30");
                expect(nizDohvacenihKrajeva).to.include("13:00");
                expect(nizDohvacenihKrajeva).to.include("15:00");
                expect(nizDohvacenihNaziva).to.include("1-11");
                expect(nizDohvacenihNaziva).to.include("1-15");
                done();
            });
    });
});

describe("Test 5", () => {
    it ("Treba ispravno azurirati sva zauzeca nakon dodavanja nove periodicne rezervacije", function(done) {
        let objekatZaPostRequest = {
            danUMjesecu: 10,
            danUSedmici: 5,
            mjesec: 0,
            sala: "1-15",
            pocetak: "13:30",
            kraj: "15:00",
            periodicnaRezervacija: true,
            imeIPrezimeOsobe: "Drugi Neko",
            ulogaOsobe: "asistent"
        };
        chai
            .request(index)
            .post("/zauzeca")
            .send(objekatZaPostRequest)
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('periodicna').with.lengthOf(2);
                expect(res.body).to.have.property('vanredna').with.lengthOf(2);
                let nizDohvacenihPeriodicnihZauzeca = res.body.periodicna;
                let nizDohvacenihDanaPeriodicno = [];
                let nizDohvacenihSemestaraPeriodicno = [];
                let nizDohvacenihPocetakaPeriodicno = [];
                let nizDohvacenihKrajevaPeriodicno = [];
                let nizDohvacenihNazivaPeriodicno = [];
                nizDohvacenihPeriodicnihZauzeca.forEach(pojedinacnoZauzece => {
                    nizDohvacenihDanaPeriodicno.push(pojedinacnoZauzece.dan);
                    nizDohvacenihSemestaraPeriodicno.push(pojedinacnoZauzece.semestar);
                    nizDohvacenihPocetakaPeriodicno.push(pojedinacnoZauzece.pocetak);
                    nizDohvacenihKrajevaPeriodicno.push(pojedinacnoZauzece.kraj);
                    nizDohvacenihNazivaPeriodicno.push(pojedinacnoZauzece.naziv);
                });
                expect(nizDohvacenihDanaPeriodicno).to.include(0);
                expect(nizDohvacenihDanaPeriodicno).to.include(4);
                expect(nizDohvacenihSemestaraPeriodicno).to.include("zimski");
                expect(nizDohvacenihSemestaraPeriodicno).to.not.include("ljetni");
                expect(nizDohvacenihPocetakaPeriodicno).to.include("13:00");
                expect(nizDohvacenihPocetakaPeriodicno).to.include("13:30");
                expect(nizDohvacenihKrajevaPeriodicno).to.include("14:00");
                expect(nizDohvacenihKrajevaPeriodicno).to.include("15:00");
                expect(nizDohvacenihNazivaPeriodicno).to.include("1-11");
                expect(nizDohvacenihNazivaPeriodicno).to.include("1-15");
                let nizDohvacenihVanrednihZauzeca = res.body.vanredna;
                let nizDohvacenihDatuma = [];
                let nizDohvacenihPocetaka = [];
                let nizDohvacenihKrajeva = [];
                let nizDohvacenihNaziva = [];
                nizDohvacenihVanrednihZauzeca.forEach(pojedinacnoZauzece => {
                    nizDohvacenihDatuma.push(pojedinacnoZauzece.datum);
                    nizDohvacenihPocetaka.push(pojedinacnoZauzece.pocetak);
                    nizDohvacenihKrajeva.push(pojedinacnoZauzece.kraj);
                    nizDohvacenihNaziva.push(pojedinacnoZauzece.naziv);
                });
                expect(nizDohvacenihDatuma).to.include("01.01.2020");
                expect(nizDohvacenihDatuma).to.include("09.01.2020");
                expect(nizDohvacenihPocetaka).to.include("12:00");
                expect(nizDohvacenihPocetaka).to.include("13:30");
                expect(nizDohvacenihKrajeva).to.include("13:00");
                expect(nizDohvacenihKrajeva).to.include("15:00");
                expect(nizDohvacenihNaziva).to.include("1-11");
                expect(nizDohvacenihNaziva).to.include("1-15");
                done();
            });
    });
});

describe("Test 6", () => {
    it ("Treba vratiti poruku greske ako ista osoba pokusa rezervisati redovni termin koji se preklapa sa postojecim", function(done) {
        let objekatZaPostRequest = {
            danUMjesecu: 10,
            danUSedmici: 5,
            mjesec: 0,
            sala: "1-15",
            pocetak: "13:50",
            kraj: "15:50",
            periodicnaRezervacija: true,
            imeIPrezimeOsobe: "Drugi Neko",
            ulogaOsobe: "asistent"
        };
        chai
            .request(index)
            .post("/zauzeca")
            .send(objekatZaPostRequest)
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.text).to.equal("Nije moguće rezervisati salu 1-15 za navedeni datum 10/01/2020 i termin od 13:50 do 15:50! " +
                 "Salu je već rezervisala osoba (asistent) Drugi Neko!");
                done();
            });
    });
});

describe("Test 7", () => {
    it ("Treba vratiti poruku greske ako druga osoba pokusa rezervisati redovni termin koji je vec rezervisala neka osoba", function(done) {
        let objekatZaPostRequest = {
            danUMjesecu: 10,
            danUSedmici: 5,
            mjesec: 0,
            sala: "1-15",
            pocetak: "13:45",
            kraj: "15:10",
            periodicnaRezervacija: true,
            imeIPrezimeOsobe: "Test Test",
            ulogaOsobe: "asistent"
        };
        chai
            .request(index)
            .post("/zauzeca")
            .send(objekatZaPostRequest)
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.text).to.equal("Nije moguće rezervisati salu 1-15 za navedeni datum 10/01/2020 i termin od 13:45 do 15:10! " +
                 "Salu je već rezervisala osoba (asistent) Drugi Neko!");
                done();
            });
    });
});

describe("Test 8", () => {
    it ("Treba vratiti poruku greske ako ista osoba pokusa rezervisati vanredni termin koji se preklapa sa postojecim", function(done) {
        let objekatZaPostRequest = {
            danUMjesecu: 9,
            danUSedmici: 4,
            mjesec: 0,
            sala: "1-15",
            pocetak: "13:30",
            kraj: "15:45",
            periodicnaRezervacija: false,
            imeIPrezimeOsobe: "Drugi Neko",
            ulogaOsobe: "asistent"
        };
        chai
            .request(index)
            .post("/zauzeca")
            .send(objekatZaPostRequest)
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.text).to.equal("Nije moguće rezervisati salu 1-15 za navedeni datum 09/01/2020 i termin od 13:30 do 15:45! " +
                 "Salu je već rezervisala osoba (asistent) Drugi Neko!");
                done();
            });
    });
});

describe("Test 9", () => {
    it ("Treba vratiti poruku greske ako druga osoba pokusa rezervisati vanredni termin koji se preklapa sa postojecim", function(done) {
        let objekatZaPostRequest = {
            danUMjesecu: 9,
            danUSedmici: 4,
            mjesec: 0,
            sala: "1-15",
            pocetak: "13:30",
            kraj: "15:00",
            periodicnaRezervacija: false,
            imeIPrezimeOsobe: "Test Test",
            ulogaOsobe: "asistent"
        };
        chai
            .request(index)
            .post("/zauzeca")
            .send(objekatZaPostRequest)
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.text).to.equal("Nije moguće rezervisati salu 1-15 za navedeni datum 09/01/2020 i termin od 13:30 do 15:00! " +
                 "Salu je već rezervisala osoba (asistent) Drugi Neko!");
                done();
            });
    });
});

describe("Test 10", () => {
    it ("Treba vratiti poruku greske ako osoba pokusa rezervisati redovni termin u istom semestru i na isti dan kao neki postojeci vanredni", function(done) {
        let objekatZaPostRequest = {
            danUMjesecu: 19,
            danUSedmici: 4,
            mjesec: 10,
            sala: "1-15",
            pocetak: "13:30",
            kraj: "15:00",
            periodicnaRezervacija: true,
            imeIPrezimeOsobe: "Test Test",
            ulogaOsobe: "asistent"
        };
        chai
            .request(index)
            .post("/zauzeca")
            .send(objekatZaPostRequest)
            .end((err, res) => {
                if (err) done(err);
                expect(res).to.have.status(200);
                expect(res.text).to.equal("Nije moguće rezervisati salu 1-15 za navedeni datum 19/11/2020 i termin od 13:30 do 15:00! " +
                 "Salu je već rezervisala osoba (asistent) Drugi Neko!");
                done();
            });
    });
});