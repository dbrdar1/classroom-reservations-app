const Sequelize = require("sequelize");

const sequelize = new Sequelize("DBWT19","root","root", {host:"127.0.0.1", dialect:"mysql", logging: false});
const db={};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import modela
db.osoblje = sequelize.import(__dirname+'/osoblje.js');
db.sala = sequelize.import(__dirname+'/sala.js');
db.termin = sequelize.import(__dirname+'/termin.js');
db.rezervacija = sequelize.import(__dirname+'/rezervacija.js');


//relacije
db.osoblje.hasMany(db.rezervacija, {as:'rezervacijeOsoblja', foreignKey: 'osoba'});

db.termin.hasOne(db.rezervacija, {foreignKey: 'termin'});

db.sala.hasMany(db.rezervacija, {as: 'rezervacijeSale', foreignKey: 'sala'});

db.osoblje.hasOne(db.sala, {foreignKey: 'zaduzenaOsoba'});


module.exports=db;