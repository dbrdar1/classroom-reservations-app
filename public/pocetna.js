let nizDosadUcitanihSlika = [];
let indexPrveSlike = 0;
let trenutnaPozicijaUSlikama = 0;

let dugmePrethodni = document.getElementById("id_dugmePrethodni");
let dugmeSljedeci = document.getElementById("id_dugmeSljedeci");
let sadrzajSaSlikamaRef = document.getElementById("id_sadrzajSaSlikama");

window.onload = function() {
    trenutnaPozicijaUSlikama = 0;
    indexPrveSlike = 0;
    dugmePrethodni.disabled = true;
    Pozivi.pozoviAjaxUcitajSlike(nizDosadUcitanihSlika);
}

dugmePrethodni.addEventListener("click", function(ev) {
    trenutnaPozicijaUSlikama--;
    indexPrveSlike -= 3;
    if (trenutnaPozicijaUSlikama == 0) dugmePrethodni.disabled = true;
    else dugmePrethodni.disabled = false;
    sadrzajSaSlikamaRef.innerHTML = "";
    for (let i = 0; i < 3; i++) {
        let indeks = indexPrveSlike + i;
        if (indeks == nizDosadUcitanihSlika.length) break;
        let trenutnaSlika = nizDosadUcitanihSlika[indeks];
        let novaSlikaElement = document.createElement("img");
        novaSlikaElement.setAttribute("src", trenutnaSlika);
        novaSlikaElement.setAttribute("alt", "Slika u galeriji");
        novaSlikaElement.classList.add("slikaUGaleriji");
        sadrzajSaSlikamaRef.appendChild(novaSlikaElement);
    }
    dugmeSljedeci.disabled = false;
}, false);

dugmeSljedeci.addEventListener("click", function(ev) {
    trenutnaPozicijaUSlikama++;
    indexPrveSlike += 3;
    if (indexPrveSlike < nizDosadUcitanihSlika.length) {
        sadrzajSaSlikamaRef.innerHTML = "";
        for (let i = 0; i < 3; i++) {
            let indeks = indexPrveSlike + i;
            if (indeks == nizDosadUcitanihSlika.length) break;
            let trenutnaSlika = nizDosadUcitanihSlika[indeks];
            let novaSlikaElement = document.createElement("img");
            novaSlikaElement.setAttribute("src", trenutnaSlika);
            novaSlikaElement.setAttribute("alt", "Slika u galeriji");
            novaSlikaElement.classList.add("slikaUGaleriji");
            sadrzajSaSlikamaRef.appendChild(novaSlikaElement);
        }
    }
    else if (indexPrveSlike == nizDosadUcitanihSlika.length) {
        Pozivi.pozoviAjaxUcitajSlike(nizDosadUcitanihSlika);
    }
    dugmePrethodni.disabled = false;
}, false);