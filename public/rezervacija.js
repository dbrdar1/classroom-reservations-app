window.onload = function() {
    Pozivi.pozoviAjaxUcitajPodatke();
    Pozivi.pozoviAjaxUcitajOsoblje();
    Pozivi.pozoviAjaxUcitajSale();
}

function uporediVremena(vrijemePrvo, vrijemeDrugo) {

    if (vrijemePrvo === vrijemeDrugo) return 0;
    let nizPrvi = vrijemePrvo.split(":");
    let nizDrugi = vrijemeDrugo.split(":");
    if (eval(nizPrvi[0]) > eval(nizDrugi[0])) {
        return 1;
    }
    else if (eval(nizPrvi[0]) === eval(nizDrugi[0]) && eval(nizPrvi[1]) > eval(nizDrugi[1])) return 1;
    else return -1;

}

let trenutniDatum = new Date();
let trenutniMjesec = trenutniDatum.getMonth();

let dugmePrethodni = document.getElementById("id_dugmePrethodni");
let dugmeSljedeci = document.getElementById("id_dugmeSljedeci");

if (trenutniMjesec === 0) dugmePrethodni.disabled = true;
else dugmePrethodni.disabled = false;
if (trenutniMjesec === 11) dugmeSljedeci.disabled = true;
else dugmeSljedeci.disabled = false;

Kalendar.iscrtajKalendar(document.getElementById("kalendar"), trenutniMjesec);

let dropdownListaSala = document.getElementById("id_listaSala");
let inputPocetak = document.getElementById("id_pocetak");
let inputKraj = document.getElementById("id_kraj");
let checkboxPeriodicnaRezervacija = document.getElementById("id_periodicnaRezervacija");
let dropdownOsobe = document.getElementById("id_listaOsoblja");

let trimovanaVrijednostInputaPocetak = inputPocetak.value.trim();
let trimovanaVrijednostInputaKraj = inputKraj.value.trim();
let vrijednostDropdownaSale = dropdownListaSala.value;
let vrijednostDropdownaOsobe = dropdownOsobe.value;

dugmePrethodni.addEventListener("click", function(ev) {
    trenutniMjesec--;
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), trenutniMjesec);
    if (trenutniMjesec === 0) dugmePrethodni.disabled = true;
    else dugmePrethodni.disabled = false;
    if (trenutniMjesec === 11) dugmeSljedeci.disabled = true;
    else dugmeSljedeci.disabled = false;
    Kalendar.obojiZauzeca(document.getElementById("kalendar"), trenutniMjesec, 
     vrijednostDropdownaSale, trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj);
    let nizDanaUKalendaru = document.getElementsByClassName("danUKalendaru");
    for (let i = 0; i < nizDanaUKalendaru.length; i++) {
        let pojedinacniDanUKalendaru = nizDanaUKalendaru[i];
        pojedinacniDanUKalendaru.addEventListener("click", function() {
    
            trimovanaVrijednostInputaPocetak = inputPocetak.value.trim();
            trimovanaVrijednostInputaKraj = inputKraj.value.trim();
            vrijednostDropdownaSale = dropdownListaSala.value;
            vrijednostCheckboxaPeriodicna = checkboxPeriodicnaRezervacija.checked;
            vrijednostDropdownaOsobe = dropdownOsobe.value;
            if (vrijednostCheckboxaPeriodicna == true && trenutniMjesec >= 6 && trenutniMjesec <= 8) return;
            if (trimovanaVrijednostInputaPocetak == null || (!trimovanaVrijednostInputaPocetak.length)) return;
            if (trimovanaVrijednostInputaKraj == null || (!trimovanaVrijednostInputaKraj.length)) return;
            if (uporediVremena(trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj) >= 0) return;
    
            //if (pojedinacniDanUKalendaru.classList.contains("zauzeta")) return;
            //else {
                let odabirKorisnika = confirm("Da li želite rezervisati ovaj termin?");
                if (odabirKorisnika == true) {
                    let danUSedmici = new Date(new Date().getFullYear(), trenutniMjesec, i + 1).getDay();
                    Pozivi.pozoviAjaxUpisiZauzece(i + 1, danUSedmici, trenutniMjesec, vrijednostDropdownaSale, 
                    trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj, vrijednostCheckboxaPeriodicna, vrijednostDropdownaOsobe);
                }
                else {
                    return;
                }
            //}
        });
    }
}, false);

dugmeSljedeci.addEventListener("click", function(ev) {
    trenutniMjesec++;
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), trenutniMjesec);
    if (trenutniMjesec === 0) dugmePrethodni.disabled = true;
    else dugmePrethodni.disabled = false;
    if (trenutniMjesec === 11) dugmeSljedeci.disabled = true;
    else dugmeSljedeci.disabled = false;
    Kalendar.obojiZauzeca(document.getElementById("kalendar"), trenutniMjesec, 
     vrijednostDropdownaSale, trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj);
    let nizDanaUKalendaru = document.getElementsByClassName("danUKalendaru");
    for (let i = 0; i < nizDanaUKalendaru.length; i++) {
        let pojedinacniDanUKalendaru = nizDanaUKalendaru[i];
        pojedinacniDanUKalendaru.addEventListener("click", function() {
    
            trimovanaVrijednostInputaPocetak = inputPocetak.value.trim();
            trimovanaVrijednostInputaKraj = inputKraj.value.trim();
            vrijednostDropdownaSale = dropdownListaSala.value;
            vrijednostCheckboxaPeriodicna = checkboxPeriodicnaRezervacija.checked;
            vrijednostDropdownaOsobe = dropdownOsobe.value;
            if (vrijednostCheckboxaPeriodicna == true && trenutniMjesec >= 6 && trenutniMjesec <= 8) return;
            if (trimovanaVrijednostInputaPocetak == null || (!trimovanaVrijednostInputaPocetak.length)) return;
            if (trimovanaVrijednostInputaKraj == null || (!trimovanaVrijednostInputaKraj.length)) return;
            if (uporediVremena(trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj) >= 0) return;
    
            //if (pojedinacniDanUKalendaru.classList.contains("zauzeta")) return;
            //else {
                let odabirKorisnika = confirm("Da li želite rezervisati ovaj termin?");
                if (odabirKorisnika == true) {
                    let danUSedmici = new Date(new Date().getFullYear(), trenutniMjesec, i + 1).getDay();
                    Pozivi.pozoviAjaxUpisiZauzece(i + 1, danUSedmici, trenutniMjesec, vrijednostDropdownaSale, 
                    trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj, vrijednostCheckboxaPeriodicna, vrijednostDropdownaOsobe);
                }
                else {
                    return;
                }
            //}
        });
    }
}, false);

dropdownListaSala.addEventListener("change", function() {
    vrijednostDropdownaSale = this.value;
    if (trimovanaVrijednostInputaPocetak == null || (!trimovanaVrijednostInputaPocetak.length)) return;
    if (trimovanaVrijednostInputaKraj == null || (!trimovanaVrijednostInputaKraj.length)) return;
    Kalendar.obojiZauzeca(document.getElementById("kalendar"), trenutniMjesec, 
     vrijednostDropdownaSale, trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj);
});

inputPocetak.addEventListener("change", function() {
    trimovanaVrijednostInputaPocetak = this.value.trim();
    if (trimovanaVrijednostInputaPocetak == null || (!trimovanaVrijednostInputaPocetak.length)) return;
    if (trimovanaVrijednostInputaKraj == null || (!trimovanaVrijednostInputaKraj.length)) return;
    vrijednostDropdownaSale = dropdownListaSala.value;
    Kalendar.obojiZauzeca(document.getElementById("kalendar"), trenutniMjesec, 
     vrijednostDropdownaSale, trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj);
});

inputKraj.addEventListener("change", function() {
    trimovanaVrijednostInputaKraj = this.value.trim();
    if (trimovanaVrijednostInputaPocetak == null || (!trimovanaVrijednostInputaPocetak.length)) return;
    if (trimovanaVrijednostInputaKraj == null || (!trimovanaVrijednostInputaKraj.length)) return;
    vrijednostDropdownaSale = dropdownListaSala.value;
    Kalendar.obojiZauzeca(document.getElementById("kalendar"), trenutniMjesec, 
     vrijednostDropdownaSale, trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj);
});

let nizDanaUKalendaru = document.getElementsByClassName("danUKalendaru");
for (let i = 0; i < nizDanaUKalendaru.length; i++) {
    let pojedinacniDanUKalendaru = nizDanaUKalendaru[i];
    pojedinacniDanUKalendaru.addEventListener("click", function() {

        trimovanaVrijednostInputaPocetak = inputPocetak.value.trim();
        trimovanaVrijednostInputaKraj = inputKraj.value.trim();
        vrijednostDropdownaSale = dropdownListaSala.value;
        vrijednostCheckboxaPeriodicna = checkboxPeriodicnaRezervacija.checked;
        vrijednostDropdownaOsobe = dropdownOsobe.value;
        if (vrijednostCheckboxaPeriodicna == true && trenutniMjesec >= 6 && trenutniMjesec <= 8) return;
        if (trimovanaVrijednostInputaPocetak == null || (!trimovanaVrijednostInputaPocetak.length)) return;
        if (trimovanaVrijednostInputaKraj == null || (!trimovanaVrijednostInputaKraj.length)) return;
        if (uporediVremena(trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj) >= 0) return;

        //if (pojedinacniDanUKalendaru.classList.contains("zauzeta")) return;
        //else {
            let odabirKorisnika = confirm("Da li želite rezervisati ovaj termin?");
            if (odabirKorisnika == true) {
                let danUSedmici = new Date(new Date().getFullYear(), trenutniMjesec, i + 1).getDay();
                Pozivi.pozoviAjaxUpisiZauzece(i + 1, danUSedmici, trenutniMjesec, vrijednostDropdownaSale, 
                 trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj, vrijednostCheckboxaPeriodicna, vrijednostDropdownaOsobe);
            }
            else {
                return;
            }
        //}
    });
}