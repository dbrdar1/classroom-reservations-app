let assert = chai.assert;
let expect = chai.expect;

describe('Kalendar', function() {
    describe('ucitajPodatke() i obojiZauzeca()', function() {

        it('treba ostaviti neobojenim, tj. zelenim sve dane ako nisu ucitani podaci', function() {
            let pronadjenoObojeno = false;
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "1-04", "13:00", "15:00");
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let okvirSaBojom = nizDanaKalendara[i].getElementsByClassName("okvirSaBojom")[0];
                let stilBoja = window.getComputedStyle(okvirSaBojom);
                if (stilBoja.backgroundColor === "rgb(231, 120, 120)") {
                    pronadjenoObojeno = true;
                    break;
                }
            }
            assert.equal(pronadjenoObojeno, false, "Ne smije biti obojen nijedan dan ako podaci nisu ucitani");
        });

        it('treba obojiti dan bez obzira na postojanje duplikata u nizu zauzeca', function() {
            let vanrednoZauzece1 = {
                datum: "03.11.2019",
                pocetak: "16:00",
                kraj: "19:00",
                naziv: "1-05",
                predavac: "Imenko Prezimenkovic"
            };
            let vanrednoZauzece2 = {
                datum: "03.11.2019",
                pocetak: "16:00",
                kraj: "19:00",
                naziv: "1-05",
                predavac: "Imenko Prezimenkovic"
            };
            let nizPeriodicnih = [];
            let nizVanrednih = [vanrednoZauzece1, vanrednoZauzece2];
            Kalendar.ucitajPodatke(nizPeriodicnih, nizVanrednih);
            let pogresnoObojeno = false;
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "1-05", "16:00", "19:00");
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let okvirSaBojom = nizDanaKalendara[i].getElementsByClassName("okvirSaBojom")[0];
                let stilBoja = window.getComputedStyle(okvirSaBojom);
                if (i === 2) {
                    if (stilBoja.backgroundColor !== "rgb(231, 120, 120)") {
                        pogresnoObojeno = true;
                        break;
                    }
                }
            }
            assert.equal(pogresnoObojeno, false, "Bez obzira na duplikat, dan treba obojiti korektno");
        });

        it('treba ostaviti neobojenim ako postoji periodicno zauzece za suprotni semestar', function() {
            let periodicnoZauzece = {
                dan: 3,
                semestar: "ljetni",
                pocetak: "13:00",
                kraj: "14:00",
                naziv: "1-08",
                predavac: "Ime Prezimenkovic"
            };
            let nizPeriodicnih = [periodicnoZauzece];
            let nizVanrednih = [];
            Kalendar.ucitajPodatke(nizPeriodicnih, nizVanrednih);
            let viskaObojeno = false;
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "1-08", "13:00", "14:00");
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let okvirSaBojom = nizDanaKalendara[i].getElementsByClassName("okvirSaBojom")[0];
                let stilBoja = window.getComputedStyle(okvirSaBojom);
                if (stilBoja.backgroundColor === "rgb(231, 120, 120)") {
                    viskaObojeno = true;
                    break;
                }
            }
            assert.equal(viskaObojeno, false, "Periodicno zauzece je u suprotnom semestru i ne odnosi se na ovaj semestar");
        });

        it('treba ostaviti neobojenim ako postoji zauzece za drugi mjesec', function() {
            let vanrednoZauzece = {
                datum: "10.10.2019",
                pocetak: "15:00",
                kraj: "17:30",
                naziv: "0-02",
                predavac: "Imenko Prezimenkovic"
            };
            let nizPeriodicnih = [];
            let nizVanrednih = [vanrednoZauzece];
            Kalendar.ucitajPodatke(nizPeriodicnih, nizVanrednih);
            let viskaObojeno = false;
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "0-02", "15:00", "17:30");
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let okvirSaBojom = nizDanaKalendara[i].getElementsByClassName("okvirSaBojom")[0];
                let stilBoja = window.getComputedStyle(okvirSaBojom);
                if (stilBoja.backgroundColor === "rgb(231, 120, 120)") {
                    viskaObojeno = true;
                    break;
                }
            }
            assert.equal(viskaObojeno, false, "Zauzece postoji ali u drugom mjesecu");
        });

        it('treba obojiti sve dane u mjesecu ako su svi termini u mjesecu zauzeti', function() {
            let periodicnoZauzece1 = {
                dan: 0,
                semestar: "zimski",
                pocetak: "08:00",
                kraj: "09:30",
                naziv: "1-02",
                predavac: "Ime Prezimenkovic"
            };
            let periodicnoZauzece2 = {
                dan: 1,
                semestar: "zimski",
                pocetak: "08:00",
                kraj: "09:30",
                naziv: "1-02",
                predavac: "Ime Prezimenkovic"
            };
            let periodicnoZauzece3 = {
                dan: 2,
                semestar: "zimski",
                pocetak: "08:00",
                kraj: "09:30",
                naziv: "1-02",
                predavac: "Ime Prezimenkovic"
            };
            let periodicnoZauzece4 = {
                dan: 3,
                semestar: "zimski",
                pocetak: "08:00",
                kraj: "09:30",
                naziv: "1-02",
                predavac: "Ime Prezimenkovic"
            };
            let periodicnoZauzece5 = {
                dan: 4,
                semestar: "zimski",
                pocetak: "08:00",
                kraj: "09:30",
                naziv: "1-02",
                predavac: "Ime Prezimenkovic"
            };
            let periodicnoZauzece6 = {
                dan: 5,
                semestar: "zimski",
                pocetak: "08:00",
                kraj: "09:30",
                naziv: "1-02",
                predavac: "Ime Prezimenkovic"
            };
            let periodicnoZauzece7 = {
                dan: 6,
                semestar: "zimski",
                pocetak: "08:00",
                kraj: "09:30",
                naziv: "1-02",
                predavac: "Ime Prezimenkovic"
            };
            let nizPeriodicnih = [periodicnoZauzece1, periodicnoZauzece2, periodicnoZauzece3, 
                periodicnoZauzece4, periodicnoZauzece5, periodicnoZauzece6, periodicnoZauzece7];
            let nizVanrednih = [];
            Kalendar.ucitajPodatke(nizPeriodicnih, nizVanrednih);
            let obojeniSviDani = true;
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "1-02", "08:00", "09:30");
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let stilDana = window.getComputedStyle(nizDanaKalendara[i]);
                let okvirSaBojom = nizDanaKalendara[i].getElementsByClassName("okvirSaBojom")[0];
                let stilBoja = window.getComputedStyle(okvirSaBojom);
                if (stilDana.display !== 'none') {
                    if (stilBoja.backgroundColor !== "rgb(231, 120, 120)") {
                        obojeniSviDani = false;
                        break;
                    }
                }
            }
            assert.equal(obojeniSviDani, true, "Svi dani u mjesecu trebaju biti obojeni");
        });

        it('treba ostati ista boja zauzeca ako se pozove obojiZauzeca dvaput uzastopno', function() {
            let vanrednoZauzece = {
                datum: "13.11.2019",
                pocetak: "15:00",
                kraj: "17:20",
                naziv: "0-03",
                predavac: "Imenko Prezimenkovic"
            };
            let nizPeriodicnih = [];
            let nizVanrednih = [vanrednoZauzece];
            Kalendar.ucitajPodatke(nizPeriodicnih, nizVanrednih);
            let dobroObojeno = true;
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "0-03", "15:00", "17:20");
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "0-03", "15:00", "17:20");
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let okvirSaBojom = nizDanaKalendara[i].getElementsByClassName("okvirSaBojom")[0];
                let stilBoja = window.getComputedStyle(okvirSaBojom);
                let stilDana = window.getComputedStyle(nizDanaKalendara[i]);
                if (stilDana.display !== 'none') {
                    if (i == 12) {
                        if (stilBoja.backgroundColor !== "rgb(231, 120, 120)") {
                            dobroObojeno = false;
                            break;
                        }
                    }
                    else {
                        if (stilBoja.backgroundColor === "rgb(231, 120, 120)") {
                            dobroObojeno = false;
                            break;
                        }
                    }
                }
            }
            assert.equal(dobroObojeno, true, "Treba ostati ista boja zauzeca");
        });

        it('trebaju se primijeniti samo posljednje ucitani podaci nakon ucitavanja novih podataka', function() {
            let vanrednoZauzece1 = {
                datum: "14.11.2019",
                pocetak: "15:00",
                kraj: "17:20",
                naziv: "0-03",
                predavac: "Imenko Prezimenkovic"
            };
            let nizPeriodicnih1 = [];
            let nizVanrednih1 = [vanrednoZauzece1];
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke(nizPeriodicnih1, nizVanrednih1);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "0-03", "15:00", "17:20");
            let vanrednoZauzece2 = {
                datum: "19.11.2019",
                pocetak: "15:00",
                kraj: "17:20",
                naziv: "0-03",
                predavac: "Imenko Prezimenkovic"
            };
            let nizPeriodicnih2 = [];
            let nizVanrednih2 = [vanrednoZauzece2];
            Kalendar.ucitajPodatke(nizPeriodicnih2, nizVanrednih2);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "0-03", "15:00", "17:20");
            let dobroObojeno = true;
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let okvirSaBojom = nizDanaKalendara[i].getElementsByClassName("okvirSaBojom")[0];
                let stilBoja = window.getComputedStyle(okvirSaBojom);
                let stilDana = window.getComputedStyle(nizDanaKalendara[i]);
                if (stilDana.display !== 'none') {
                    if (i == 18) {
                        if (stilBoja.backgroundColor !== "rgb(231, 120, 120)") {
                            dobroObojeno = false;
                            break;
                        }
                    }
                    else {
                        if (stilBoja.backgroundColor === "rgb(231, 120, 120)") {
                            dobroObojeno = false;
                            break;
                        }
                    }
                }
            }
            assert.equal(dobroObojeno, true, "Trebaju biti obojeni samo dani iz posljednje ucitanih podataka");
        });

        // Dva dodatna testa

        it('treba ostati neobojeno ako se pozove obojiZauzeca sa pocetkom koji je tacno onda kad je kraj stvarnog ucitanog zauzeca', function() {
            let vanrednoZauzece1 = {
                datum: "15.11.2019",
                pocetak: "15:00",
                kraj: "17:20",
                naziv: "0-04",
                predavac: "Imenko Prezimenkovic"
            };
            let nizPeriodicnih1 = [];
            let nizVanrednih1 = [vanrednoZauzece1];
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke(nizPeriodicnih1, nizVanrednih1);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "0-04", "17:20", "18:20");
            let dobroObojeno = true;
            let petnaestiDanUKalendaru = document.getElementsByClassName("danUKalendaru")[14];
            let okvirSaBojom = petnaestiDanUKalendaru.getElementsByClassName("okvirSaBojom")[0];
            let stilBoja = window.getComputedStyle(okvirSaBojom);
            if (stilBoja.backgroundColor === "rgb(231, 120, 120)") dobroObojeno = false;
            assert.equal(dobroObojeno, true, "Treba ostati neobojeno ako nema preklapanja");
        });

        it('treba ostaviti sve neobojenim ako se obojiZauzeca pozove sa krajem koji je prije pocetka', function() {
            let vanrednoZauzece1 = {
                datum: "15.11.2019",
                pocetak: "16:30",
                kraj: "18:20",
                naziv: "0-07",
                predavac: "Imenko Prezimenkovic"
            };
            let nizPeriodicnih1 = [];
            let nizVanrednih1 = [vanrednoZauzece1];
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke(nizPeriodicnih1, nizVanrednih1);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "0-07", "17:00", "16:55");
            let dobroObojeno = true;
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let okvirSaBojom = nizDanaKalendara[i].getElementsByClassName("okvirSaBojom")[0];
                let stilBoja = window.getComputedStyle(okvirSaBojom);
                let stilDana = window.getComputedStyle(nizDanaKalendara[i]);
                if (stilDana.display !== 'none') {
                    if (stilBoja.backgroundColor === "rgb(231, 120, 120)") {
                        dobroObojeno = false;
                        break;
                    }
                }
            }
            assert.equal(dobroObojeno, true, "Treba ostati neobojeno ako je zadat kraj prije pocetka");
        });
    });
});


describe('Kalendar', function() {
    describe('iscrtajKalendar()', function() {
        it('treba iscrtati 30 dana u kalendaru za april', function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 3);
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            let brojPrikazanihDanaUKalendaru = 0;
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let stilprikaza = window.getComputedStyle(nizDanaKalendara[i]);
                if (stilprikaza.display !== 'none') brojPrikazanihDanaUKalendaru++;
            }
            assert.equal(brojPrikazanihDanaUKalendaru, 30,"Broj prikazanih dana aprila treba biti 30");
        });

        it('treba iscrtati 31 dan u kalendaru za august', function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 7);
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            let brojPrikazanihDanaUKalendaru = 0;
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let stilprikaza = window.getComputedStyle(nizDanaKalendara[i]);
                if (stilprikaza.display !== 'none') brojPrikazanihDanaUKalendaru++;
            }
            assert.equal(brojPrikazanihDanaUKalendaru, 31, "Broj prikazanih dana augusta treba biti 31");
        });

        it('treba biti 1. dan trenutnog mjeseca novembra u petak', function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            let petak = document.getElementsByClassName("nazivDana")[4];
            let prviDanMjeseca = document.getElementsByClassName("danUKalendaru")[0];
            let pozicijaPetka = window.getComputedStyle(petak).gridColumn;
            let pozicijaPrvogDanaUMjesecu = window.getComputedStyle(prviDanMjeseca).gridColumn;
            expect(pozicijaPrvogDanaUMjesecu).to.be.oneOf([pozicijaPetka, "5 / auto"]);
        });

        it('treba biti 30. dan trenutnog mjeseca novembra u subotu', function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            let subota = document.getElementsByClassName("nazivDana")[5];
            let tridesetiDanMjeseca = document.getElementsByClassName("danUKalendaru")[29];
            let pozicijaSubote = window.getComputedStyle(subota).gridColumn;
            let pozicijaTridesetogDanaUMjesecu = window.getComputedStyle(tridesetiDanMjeseca).gridColumn;
            assert.equal(pozicijaSubote, pozicijaTridesetogDanaUMjesecu, "Trideseti dan novembra treba biti u subotu");
        });

        it('treba prikazati od 1. do 31. januara pocevsi od utorka', function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 0);
            let utorak = document.getElementsByClassName("nazivDana")[1];
            let prviDanMjeseca = document.getElementsByClassName("danUKalendaru")[0];
            let pozicijaUtorka = window.getComputedStyle(utorak).gridColumn;
            let pozicijaPrvogDanaUMjesecu = window.getComputedStyle(prviDanMjeseca).gridColumn;
            expect(pozicijaPrvogDanaUMjesecu).to.be.oneOf([pozicijaUtorka, "2 / auto"]);
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            let nizRednihBrojevaDana = document.getElementsByClassName("okvirSaTekstom");
            let brojPrikazanihDanaUKalendaru = 0;
            for (let i = 0; i < 31; i++) {
                let tekstRednogBrojaDana = nizRednihBrojevaDana[i].innerHTML;
                assert.equal(tekstRednogBrojaDana, i + 1, "Trebaju ici brojevi redom od 1 do 31 u januaru");
                if (window.getComputedStyle(nizDanaKalendara[i]).display !== 'none') brojPrikazanihDanaUKalendaru++;
            }
            assert.equal(brojPrikazanihDanaUKalendaru, 31, "Broj prikazanih dana januara treba biti 31");
        });

        // Dodatna dva testa

        it('treba iscrtati 28 dana u kalendaru za februar', function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 1);
            let nizDanaKalendara = document.getElementsByClassName("danUKalendaru");
            let brojPrikazanihDanaUKalendaru = 0;
            for (let i = 0; i < nizDanaKalendara.length; i++) {
                let stilprikaza = window.getComputedStyle(nizDanaKalendara[i]);
                if (stilprikaza.display !== 'none') brojPrikazanihDanaUKalendaru++;
            }
            assert.equal(brojPrikazanihDanaUKalendaru, 28, "Broj dana februara 2019. je 28");
        });

        it('treba biti 17. dan oktobra u cetvrtak', function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 9);
            let cetvrtak = document.getElementsByClassName("nazivDana")[3];
            let sedamnaestiDanMjeseca = document.getElementsByClassName("danUKalendaru")[16];
            let pozicijaCetvrtka = window.getComputedStyle(cetvrtak).gridColumn;
            let pozicijaSedamnaestogDanaUMjesecu = window.getComputedStyle(sedamnaestiDanMjeseca).gridColumn;
            assert.equal(pozicijaCetvrtka, pozicijaSedamnaestogDanaUMjesecu, "Sedamnaesti dan oktobra treba biti u cetvrtak");
        });
    });
});