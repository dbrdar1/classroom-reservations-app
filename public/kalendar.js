let Kalendar = (function() {

    var periodicnaZauzeca = [];
    var vanrednaZauzeca = [];
    var listaNazivaMjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];


    function uporediVremena(vrijemePrvo, vrijemeDrugo) {

        if (vrijemePrvo === vrijemeDrugo) return 0;
        let nizPrvi = vrijemePrvo.split(":");
        let nizDrugi = vrijemeDrugo.split(":");
        if (eval(nizPrvi[0]) > eval(nizDrugi[0])) {
            return 1;
        }
        else if (eval(nizPrvi[0]) === eval(nizDrugi[0]) && eval(nizPrvi[1]) > eval(nizDrugi[1])) return 1;
        else return -1;

    }


    function jeLiVrijemeIzmedjuDrugaDva(prvoVrijeme, drugoVrijeme, treceVrijeme) {

        return uporediVremena(prvoVrijeme, drugoVrijeme) >= 0 && uporediVremena(prvoVrijeme, treceVrijeme) <= 0;

    }


    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
        
        let trenutnaGodina = new Date().getFullYear();
        let brojDanaMjeseca = new Date(trenutnaGodina, mjesec + 1, 0).getDate();
        let i;
        for (i = 0; i < brojDanaMjeseca; i++) {
            kalendarRef.children[0].children[1].children[i + 7].classList.remove("zauzeta");
            kalendarRef.children[0].children[1].children[i + 7].classList.add("slobodna");
        }
        if (uporediVremena(pocetak, kraj) > 0) return;
        // Bojenje periodicnih zauzeca
        let filtriraniNizPeriodicnihZauzeca = periodicnaZauzeca.filter(function(zauzece) {
            return zauzece.naziv === sala 
             && (jeLiVrijemeIzmedjuDrugaDva(zauzece.pocetak, pocetak, kraj) || jeLiVrijemeIzmedjuDrugaDva(zauzece.kraj, pocetak, kraj)
              || jeLiVrijemeIzmedjuDrugaDva(pocetak, zauzece.pocetak, zauzece.kraj) || jeLiVrijemeIzmedjuDrugaDva(kraj, zauzece.pocetak, zauzece.kraj))
             && ((zauzece.semestar === "zimski" && (mjesec === 0 || mjesec >= 9 && mjesec <= 11)) 
              || (zauzece.semestar === "ljetni" && (mjesec >= 1 && mjesec <= 5))) 
             && (!((uporediVremena(zauzece.kraj, pocetak) == 0 && uporediVremena(zauzece.pocetak, zauzece.kraj) <= 0) 
              || (uporediVremena(kraj, zauzece.pocetak) == 0 && uporediVremena(pocetak, kraj) <= 0)));
        });
        if (filtriraniNizPeriodicnihZauzeca.length === 0) {
            let k;
            for (k = 0; k < brojDanaMjeseca; k++) {
                kalendarRef.children[0].children[1].children[k + 7].classList.remove("zauzeta");
                kalendarRef.children[0].children[1].children[k + 7].classList.add("slobodna");
            }
        }
        for (i = 0; i < filtriraniNizPeriodicnihZauzeca.length; i++) {
            let danZauzeca = filtriraniNizPeriodicnihZauzeca[i].dan;
            let j;
            for (j = 0; j < brojDanaMjeseca; j++) {
                let danSedmice = new Date(trenutnaGodina, mjesec, j + 1).getDay();
                if (danSedmice === 0) danSedmice = 7;
                if (danSedmice === danZauzeca + 1) {
                    kalendarRef.children[0].children[1].children[j + 7].classList.remove("slobodna");
                    kalendarRef.children[0].children[1].children[j + 7].classList.add("zauzeta");
                }
            }
        }
        // Bojenje vanrednih zauzeca
        let filtriraniNizVanrednihZauzeca = vanrednaZauzeca.filter(function(zauzece) {
            let nizRazdvojen = zauzece.datum.split(".");
            nizRazdvojen.reverse();
            let noviDatumFormat = nizRazdvojen.join("-");
            let datumZauzeca = new Date(noviDatumFormat);
            return datumZauzeca.getFullYear() == trenutnaGodina 
             && datumZauzeca.getMonth() == mjesec
             && zauzece.naziv === sala 
             && (jeLiVrijemeIzmedjuDrugaDva(zauzece.pocetak, pocetak, kraj) || jeLiVrijemeIzmedjuDrugaDva(zauzece.kraj, pocetak, kraj)
              || jeLiVrijemeIzmedjuDrugaDva(pocetak, zauzece.pocetak, zauzece.kraj) || jeLiVrijemeIzmedjuDrugaDva(kraj, zauzece.pocetak, zauzece.kraj))
             && (!((uporediVremena(zauzece.kraj, pocetak) == 0 && uporediVremena(zauzece.pocetak, zauzece.kraj) <= 0) 
              || (uporediVremena(kraj, zauzece.pocetak) == 0 && uporediVremena(pocetak, kraj) <= 0)));
        });
        if (filtriraniNizVanrednihZauzeca.length === 0) return;
        for (i = 0; i < filtriraniNizVanrednihZauzeca.length; i++) {
            let nizRazdvojen = filtriraniNizVanrednihZauzeca[i].datum.split(".");
            nizRazdvojen.reverse();
            let noviDatumFormat = nizRazdvojen.join("-");
            let datumFiltriranogZauzeca = new Date(noviDatumFormat);
            let danZauzecaUMjesecu = datumFiltriranogZauzeca.getDate();
            let indeksZaObojitiCrveno = danZauzecaUMjesecu + 6;
            kalendarRef.children[0].children[1].children[indeksZaObojitiCrveno].classList.remove("slobodna");
            kalendarRef.children[0].children[1].children[indeksZaObojitiCrveno].classList.add("zauzeta");
        }

    }


    function ucitajPodatkeImpl(periodicna, vanredna) {

        periodicnaZauzeca = periodicna;
        vanrednaZauzeca = vanredna;

    }


    function iscrtajKalendarImpl(kalendarRef, mjesec) {

        if (mjesec < 0 || mjesec > 11) return;

        let sadasnjiDatum = new Date();
        let trenutnaGodina = sadasnjiDatum.getFullYear();
        let prviDatumUMjesecu = new Date(trenutnaGodina, mjesec, 1);
        let prviDanUMjesecu = prviDatumUMjesecu.getDay();
        if (prviDanUMjesecu === 0) prviDanUMjesecu = 7;
        let brojDanaMjeseca = new Date(trenutnaGodina, mjesec + 1, 0).getDate();

        kalendarRef.innerHTML = "";

        let okvirKalendara = document.createElement("div");
        okvirKalendara.setAttribute("id", "id_okvirKalendara");
        okvirKalendara.classList.add("okvirKalendara");

        let nazivMjeseca = document.createElement("label");
        nazivMjeseca.setAttribute("id", "id_nazivMjeseca");
        nazivMjeseca.appendChild(document.createTextNode(listaNazivaMjeseci[mjesec]));
        okvirKalendara.appendChild(nazivMjeseca);

        let gridOkvir = document.createElement("div");
        gridOkvir.classList.add("gridOkvir");

        let nizNazivaDana = ["PON", "UTO", "SRI", "ČET", "PET", "SUB", "NED"];
        for (let i = 0; i < nizNazivaDana.length; i++) {
            let nazivDana = document.createElement("div");
            nazivDana.classList.add("nazivDana");
            nazivDana.appendChild(document.createTextNode(nizNazivaDana[i]));
            gridOkvir.appendChild(nazivDana);
        }

        for (let i = 0; i < brojDanaMjeseca; i++) {
            let danUKalendaru = document.createElement("div");
            danUKalendaru.classList.add("danUKalendaru");
            danUKalendaru.classList.add("slobodna");
            if (i === 0) {
                danUKalendaru.classList.add("prviDanMjeseca");
                danUKalendaru.style.gridColumnStart = prviDanUMjesecu;
            }
            let okvirSaTekstom = document.createElement("div");
            okvirSaTekstom.classList.add("okvirSaTekstom");
            okvirSaTekstom.appendChild(document.createTextNode(i + 1));
            let okvirSaBojom = document.createElement("div");
            okvirSaBojom.classList.add("okvirSaBojom");
            danUKalendaru.appendChild(okvirSaTekstom);
            danUKalendaru.appendChild(okvirSaBojom);
            gridOkvir.appendChild(danUKalendaru);
        }

        okvirKalendara.appendChild(gridOkvir);

        kalendarRef.appendChild(okvirKalendara);

    }


    return {
        obojiZauzeca: obojiZauzecaImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        iscrtajKalendar: iscrtajKalendarImpl
    }
}());