let Pozivi = (function() {

    function pozoviAjaxUcitajPodatkeImpl() {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                let vraceniJsonZauzeca = ajax.responseText;
                console.log(vraceniJsonZauzeca);
                let vraceniObjekatZauzeca = JSON.parse(vraceniJsonZauzeca);
                let nizPeriodicnih = vraceniObjekatZauzeca.periodicna;
                let nizVanrednih = vraceniObjekatZauzeca.vanredna;
                Kalendar.ucitajPodatke(nizPeriodicnih, nizVanrednih);
            }
            if (ajax.readyState == 4 && ajax.status == 404) {
                return "Greska: nepoznat URL!";
            }
        }
        ajax.open("GET", "zauzeca", true);
        ajax.send();
    }

    function pozoviAjaxUpisiZauzeceImpl(danUMjesecu, danUSedmici, mjesec, sala, pocetak, kraj, periodicnaRezervacija, podaciOsobe) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                if (ajax.responseText.includes("Nije moguće")) {
                    alert(ajax.responseText);
                }
                else {
                    let vraceniJsonZauzeca = ajax.responseText;
                    let vraceniObjekatZauzeca = JSON.parse(vraceniJsonZauzeca);
                    let nizPeriodicnih = vraceniObjekatZauzeca.periodicna;
                    let nizVanrednih = vraceniObjekatZauzeca.vanredna;
                    Kalendar.ucitajPodatke(nizPeriodicnih, nizVanrednih);
                    trimovanaVrijednostInputaPocetak = inputPocetak.value.trim();
                    trimovanaVrijednostInputaKraj = inputKraj.value.trim();
                    vrijednostDropdownaSale = dropdownListaSala.value;
                    vrijednostCheckboxaPeriodicna = checkboxPeriodicnaRezervacija.checked;
                    if (trimovanaVrijednostInputaPocetak == null || (!trimovanaVrijednostInputaPocetak.length)) return;
                    if (trimovanaVrijednostInputaKraj == null || (!trimovanaVrijednostInputaKraj.length)) return;
                    Kalendar.obojiZauzeca(document.getElementById("kalendar"), trenutniMjesec, 
                     vrijednostDropdownaSale, trimovanaVrijednostInputaPocetak, trimovanaVrijednostInputaKraj);
                }
            }
            if (ajax.readyState == 4 && ajax.status == 404) {
                return "Greska: nepoznat URL!";
            }
        }
        ajax.open("POST", "zauzeca", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        let niz = podaciOsobe.split(",");
        let imeIPrezimeOsobe = niz[0];
        let ulogaOsobe = niz[1];
        let objekatZaPostRequest = {
            danUMjesecu: danUMjesecu,
            danUSedmici: danUSedmici,
            mjesec: mjesec,
            sala: sala,
            pocetak: pocetak,
            kraj: kraj,
            periodicnaRezervacija: periodicnaRezervacija,
            imeIPrezimeOsobe: imeIPrezimeOsobe,
            ulogaOsobe: ulogaOsobe
        }
        ajax.send(JSON.stringify(objekatZaPostRequest));
    }

    function pozoviAjaxUcitajSlikeImpl(nizVecUcitanih) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                let vraceniJsonSlika = ajax.responseText;
                let vraceniObjekatSlika = JSON.parse(vraceniJsonSlika);
                let nizVracenihSlika = vraceniObjekatSlika.nizSlika;
                let sadrzajSaSlikamaRef = document.getElementById("id_sadrzajSaSlikama");
                sadrzajSaSlikamaRef.innerHTML = "";
                for (let i = 0; i < nizVracenihSlika.length; i++) {
                    let trenutnaSlika = nizVracenihSlika[i];
                    nizDosadUcitanihSlika.push(trenutnaSlika);
                    let novaSlikaElement = document.createElement("img");
                    novaSlikaElement.setAttribute("src", trenutnaSlika);
                    novaSlikaElement.setAttribute("alt", "Slika u galeriji");
                    novaSlikaElement.classList.add("slikaUGaleriji");
                    sadrzajSaSlikamaRef.appendChild(novaSlikaElement);
                }
                if (vraceniObjekatSlika.finalneSlikePoslane) dugmeSljedeci.disabled = true;
                else dugmeSljedeci.disabled = false;
            }
            if (ajax.readyState == 4 && ajax.status == 404) {
                return "Greska: nepoznat URL!";
            }
        }
        ajax.open("POST", "slike", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        let objekatZaPostRequest = {
            nizVecPostojecihSlika: nizVecUcitanih
        }
        ajax.send(JSON.stringify(objekatZaPostRequest));
    }

    function pozoviAjaxUcitajOsobljeImpl() {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                let vraceniJsonOsoblja = ajax.responseText;
                let vraceniObjekatOsoblja = JSON.parse(vraceniJsonOsoblja);
                let nizVracenogOsoblja = vraceniObjekatOsoblja.nizOsoblja;
                let selectOsoblje = document.getElementById("id_listaOsoblja");
                selectOsoblje.innerHTML = "";
                for (let i = 0; i < nizVracenogOsoblja.length; i++) {
                    let trenutnaOsoba = nizVracenogOsoblja[i];
                    let novaOsobaElement = document.createElement("option");
                    let trenutnaOsobaString = trenutnaOsoba.ime + " " + trenutnaOsoba.prezime + "," + trenutnaOsoba.uloga;
                    novaOsobaElement.setAttribute("value", trenutnaOsobaString);
                    novaOsobaElement.appendChild(document.createTextNode(trenutnaOsobaString));
                    selectOsoblje.appendChild(novaOsobaElement);
                }
            }
            if (ajax.readyState == 4 && ajax.status == 404) {
                return "Greska: nepoznat URL!";
            }
        }
        ajax.open("GET", "osoblje", true);
        ajax.send();
    }

    function pozoviAjaxUcitajSaleImpl() {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                let vraceniJsonSala = ajax.responseText;
                let vraceniObjekatSala = JSON.parse(vraceniJsonSala);
                let nizVracenihSala = vraceniObjekatSala.nizSala;
                let selectSale = document.getElementById("id_listaSala");
                selectSale.innerHTML = "";
                for (let i = 0; i < nizVracenihSala.length; i++) {
                    let trenutnaSala = nizVracenihSala[i];
                    let novaSalaElement = document.createElement("option");
                    let trenutnaSalaString = trenutnaSala.naziv;
                    novaSalaElement.setAttribute("value", trenutnaSalaString);
                    novaSalaElement.appendChild(document.createTextNode(trenutnaSalaString));
                    selectSale.appendChild(novaSalaElement);
                }
            }
            if (ajax.readyState == 4 && ajax.status == 404) {
                return "Greska: nepoznat URL!";
            }
        }
        ajax.open("GET", "sale", true);
        ajax.send();
    }

    function postaviPeriodicniAjax30sekundi() {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                console.log("Izvrsena periodicna provjera!");
                let vraceniJsonPozicijaOsoba = ajax.responseText;
                let vraceniObjekatPozicijaOsoba = JSON.parse(vraceniJsonPozicijaOsoba);
                let nizVracenihTrojki = vraceniObjekatPozicijaOsoba.nizTrojki;
                let tabelaPozicijaOsoba = document.getElementById("id_tabelaOsoba");
                tabelaPozicijaOsoba.innerHTML = "";
                let redTabeleElement = document.createElement("tr");
                let naslovDrugeKoloneElement = document.createElement("th");
                naslovDrugeKoloneElement.appendChild(document.createTextNode("Ime i prezime"));
                let naslovTreceKoloneElement = document.createElement("th");
                naslovTreceKoloneElement.appendChild(document.createTextNode("Uloga"));
                let naslovCetvrteKoloneElement = document.createElement("th");
                naslovCetvrteKoloneElement.appendChild(document.createTextNode("Gdje je trenutno"));
                redTabeleElement.appendChild(naslovDrugeKoloneElement);
                redTabeleElement.appendChild(naslovTreceKoloneElement);
                redTabeleElement.appendChild(naslovCetvrteKoloneElement);
                tabelaPozicijaOsoba.appendChild(redTabeleElement);
                for (let i = 0; i < nizVracenihTrojki.length; i++) {
                    let trenutnaTrojka = nizVracenihTrojki[i];
                    let redTabeleElement = document.createElement("tr");
                    let imeIPrezimeElement = document.createElement("td");
                    imeIPrezimeElement.appendChild(document.createTextNode(trenutnaTrojka.imeIPrezime));
                    let ulogaElement = document.createElement("td");
                    ulogaElement.appendChild(document.createTextNode(trenutnaTrojka.uloga));
                    let gdjeJeTrenutnoElement = document.createElement("td");
                    gdjeJeTrenutnoElement.appendChild(document.createTextNode(trenutnaTrojka.gdjeJeTrenutno));
                    redTabeleElement.appendChild(imeIPrezimeElement);
                    redTabeleElement.appendChild(ulogaElement);
                    redTabeleElement.appendChild(gdjeJeTrenutnoElement);
                    tabelaPozicijaOsoba.appendChild(redTabeleElement);
                }
            }
            if (ajax.readyState == 4 && ajax.status == 404) {
                return "Greska: nepoznat URL!";
            }
        }
        ajax.open("GET", "pozicijeosoba", true);
        ajax.send();
    }

    function pozoviAjaxUcitajPozicijeOsobaImpl() {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                let vraceniJsonPozicijaOsoba = ajax.responseText;
                let vraceniObjekatPozicijaOsoba = JSON.parse(vraceniJsonPozicijaOsoba);
                let nizVracenihTrojki = vraceniObjekatPozicijaOsoba.nizTrojki;
                let tabelaPozicijaOsoba = document.getElementById("id_tabelaOsoba");
                tabelaPozicijaOsoba.innerHTML = "";
                let redTabeleElement = document.createElement("tr");
                let naslovDrugeKoloneElement = document.createElement("th");
                naslovDrugeKoloneElement.appendChild(document.createTextNode("Ime i prezime"));
                let naslovTreceKoloneElement = document.createElement("th");
                naslovTreceKoloneElement.appendChild(document.createTextNode("Uloga"));
                let naslovCetvrteKoloneElement = document.createElement("th");
                naslovCetvrteKoloneElement.appendChild(document.createTextNode("Gdje je trenutno"));
                redTabeleElement.appendChild(naslovDrugeKoloneElement);
                redTabeleElement.appendChild(naslovTreceKoloneElement);
                redTabeleElement.appendChild(naslovCetvrteKoloneElement);
                tabelaPozicijaOsoba.appendChild(redTabeleElement);
                for (let i = 0; i < nizVracenihTrojki.length; i++) {
                    let trenutnaTrojka = nizVracenihTrojki[i];
                    let redTabeleElement = document.createElement("tr");
                    let imeIPrezimeElement = document.createElement("td");
                    imeIPrezimeElement.appendChild(document.createTextNode(trenutnaTrojka.imeIPrezime));
                    let ulogaElement = document.createElement("td");
                    ulogaElement.appendChild(document.createTextNode(trenutnaTrojka.uloga));
                    let gdjeJeTrenutnoElement = document.createElement("td");
                    gdjeJeTrenutnoElement.appendChild(document.createTextNode(trenutnaTrojka.gdjeJeTrenutno));
                    redTabeleElement.appendChild(imeIPrezimeElement);
                    redTabeleElement.appendChild(ulogaElement);
                    redTabeleElement.appendChild(gdjeJeTrenutnoElement);
                    tabelaPozicijaOsoba.appendChild(redTabeleElement);
                }
                setInterval(function() { postaviPeriodicniAjax30sekundi(); }, 30000);
            }
            if (ajax.readyState == 4 && ajax.status == 404) {
                return "Greska: nepoznat URL!";
            }
        }
        ajax.open("GET", "pozicijeosoba", true);
        ajax.send();
    }

    return {
        pozoviAjaxUcitajPodatke: pozoviAjaxUcitajPodatkeImpl,
        pozoviAjaxUpisiZauzece: pozoviAjaxUpisiZauzeceImpl,
        pozoviAjaxUcitajSlike: pozoviAjaxUcitajSlikeImpl,
        pozoviAjaxUcitajOsoblje: pozoviAjaxUcitajOsobljeImpl,
        pozoviAjaxUcitajSale: pozoviAjaxUcitajSaleImpl,
        pozoviAjaxUcitajPozicijeOsoba: pozoviAjaxUcitajPozicijeOsobaImpl
    }
}());